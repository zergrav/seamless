# Seamless.ai Company Domain Finder

This project is for application to seamless.ai written by Chris Sparks -- that's me!

## Prerequisites

* **Node.js v6.11.5**
* **npm 3.10.10**

## Running the Code

Steps to run the code _(already transpiled)_:

1. Clone the repository to your local computer.
1. Open a terminal window and navigate to the newly-created directory.
1. Install dependencies of the project via running `npm install .`
1. Run the already transpiled code under 'public' via executing the Bash script './start.sh'
    * This program will print both to STDOUT as well as a local `output.log` file for later reference if needed.

Steps to run the code _(including the build step)_:

1. Clone the repository to your local computer.
1. Open a terminal window and navigate to the newly-created directory.
1. Install dependencies of the project via running `npm install .`
1. Build the project via running `gulp build` creating a fresh installation of files into the production `public` folder.
1. Run the **newly transpiled** code under 'public' via executing the Bash script './start.sh'
    * This program will print both to STDOUT as well as a local `output.log` file for later reference if needed.


