
var gulp = require('gulp');

var rename = require('gulp-rename');

var babel = require('gulp-babel');

var util = require('util');

var webpack = require('webpack-stream');

var named = require('vinyl-named');

var sass = require('gulp-sass');

var runSequence = require('run-sequence');

gulp.task('dummy', function() {
  console.log("In dummy task");
});

gulp.task('install-react', () => {
  return gulp.src([
    'node_modules/react/cjs/react.*.js',
    'node_modules/react-dom/cjs/react-dom.*.js',
    ])
    .pipe(rename( (path) => {
      console.log(
        util.inspect(path)
      );
      path.basename = path.basename.replace(/[.](development|production)/, '');
    }))
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(gulp.dest('src/js/'));
  }
);

gulp.task('preprocess-sass', () => {
  return gulp.src([
    'src/css/style.scss'
  ])
  .pipe(sass())
  .pipe(gulp.dest('build/css/'));
});

gulp.task('install-css', () => {
  gulp.src([
    'build/css/style.css'
  ])
  .pipe(gulp.dest('public/css/'));
});

gulp.task('make-server', () => {
    gulp.src([
      'src/services.jsx',
      'src/index.jsx'
    ])
    .pipe(rename( (path) => {
      path.extension = 'js';
    }))
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(gulp.dest('build/'));
  }
);

gulp.task('install-server', () => {
    gulp.src(
      'build/*.js'
    )
    .pipe(gulp.dest('public/'));
  }
);

gulp.task('transpile', () => {
    gulp.src([
      'src/app.jsx',
      'src/init.jsx'
    ])
    .pipe(babel({
        presets: ['env']
    }))
    .pipe(gulp.dest('build/js/'));
    }
);

gulp.task('pack', () => {
    gulp.src([
      'build/js/app.js',
      'build/js/init.js'
    ])
    .pipe(webpack({
      output: {
        filename: 'bundle.js'
      }
    }))
    .pipe(gulp.dest('public/js/'));
    }
);

gulp.task('build-css', () => {
  runSequence('preprocess-sass', 'install-css');
});

gulp.task('build-js', () => {
  runSequence( 'transpile', 'pack', 'make-server', 'install-server');
})

gulp.task('build', ['build-css', 'build-js']);

gulp.task('default', [ 'build' ]);
