'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _https = require('https');

var _https2 = _interopRequireDefault(_https);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _services = require('./services');

var _services2 = _interopRequireDefault(_services);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('isomorphic-fetch');

var app = (0, _express2.default)();

// configuration parameters for the web server and google custom search
// @NOTE: Normally, I would place the CSE-identification variables
//        within an "environment-altering" script that would not
//        necessarily be pushed to the Git repository.
var CFG = {
  port: 3001,
  cse: {
    url: "https://www.googleapis.com/customsearch/v1",
    apiKey: "AIzaSyBXGkVilmqEN0KSDAaQy1BlVVIA8r4nS6w",
    cexId: "009637816073108880163:nfsysoqnztc"
  }
};

// define static routes to expose
app.use(_express2.default.static('public/js', {}));
app.use(_express2.default.static('public/css', {}));

// define many entry point, and serve the 'index.html' file as the contents
app.get('/', function (req, res) {
  res.sendFile('index.html', { root: './public' }, function (err) {
    if (err) {
      console.log("Encountered error sending file::\n");
      console.log(_util2.default.inspect(err));
    }
  });
});

// the workhorse API method, taking a GET request of 'company' variables and
// passing them into the "batch" function for the CSE service; within that
// method, each company corresponds to another API request to Google CSE
app.get('/url', function (req, res) {
  // ensure any errors not caught within a "promise chain" don't percolate
  // up the stack further than this scope
  try {
    // set response content-type, since we know it's going to be serving JSON
    res.set('Content-Type', 'application/json');
    // instantiate the custom 'GoogleCustomSearchService' class, passing in
    // the API configuration parameters
    var searchService = new _services2.default(CFG['cse']['url'], CFG['cse']['apiKey'], CFG['cse']['cexId']);
    var qstr = req.query;
    // if query string indicates presence of required 'company', potentially multi-valued variable
    if (qstr.hasOwnProperty('companies')) {
      var companies = qstr.companies;
      // ensure the provided 'company' GET variable is NOT empty
      if (companies.length) {
        // if more than 25 companies provided, cut off the values at 25 per the project requirement limit
        if (companies.length > 25) {
          // @NOTE: Under normal conditions, this would likely be first handled by error-checking
          //        on the front-end, within the UI, to avoid unnecessary server calls and provide
          //        a "quicker", and therefore better, user experience.
          companies = companies.slice(0, 25);
        }
        // perform the batch query to Google CSE, passing the companies and the 'res'ponse object,
        // as the results from the query will completely fill out / handle the response object.
        searchService.batchQuery(companies, res);
      }
    } else {
      console.log("[WARNING] No 'companies' provided to url '/url' service.");
    }
  } catch (err) {
    // @NOTE: Ideally would handle this with something more streamlined, likely including an
    //        always-persistent "status" flag or equivalent within the responses, so the
    //        UI can act accordingly to show the error.
    res.json({ status: false });
  }
});

app.listen(CFG.port, function () {
  return console.log('Listening on port ' + CFG['port']);
});