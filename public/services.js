'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _https = require('https');

var _https2 = _interopRequireDefault(_https);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

require('isomorphic-fetch');

var GoogleCustomSearchService = function () {
  function GoogleCustomSearchService(url, apiKey, cexId) {
    _classCallCheck(this, GoogleCustomSearchService);

    this._url = url;
    this._apiKey = apiKey;
    this._cexId = cexId;
    this._NUM_RESULTS = 1;
  }

  _createClass(GoogleCustomSearchService, [{
    key: 'querySingle',
    value: function querySingle(company, outerRes) {
      var cacheMap = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      var cache = cacheMap;
      var companyStr = company;
      var companyQuery = '"' + encodeURIComponent(company.trim()) + '"';
      var requestUrl = this.serviceUrl + '&q=' + companyQuery;
      var promise = new Promise(function (resolve, reject) {
        var data = "";
        _https2.default.request(requestUrl, function (res) {
          res.on('data', function (d) {
            data += d;
          });
          res.on('end', function () {
            if (cacheMap === null) {}
            var dataObj = JSON.parse(data);
            dataObj['__companyName'] = companyStr;
            resolve(dataObj);
          });
        }).on('error', function (e) {
          reject('Received Promisifid query error: ' + String(e));
        }).end();
      });
      return promise;
    }
  }, {
    key: 'batchQuery',
    value: function batchQuery(companies, outerRes) {
      var collector = new Map();
      var pArr = [];
      var ores = outerRes;

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = companies[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var company = _step.value;

          var qpromise = this.querySingle(company, outerRes, collector);
          pArr.push(qpromise);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      var companyCount = companies.length;

      Promise.all(pArr).then(function (values) {
        var results = { data: [], length: 0 };
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
          for (var _iterator2 = values[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var result = _step2.value;

            results['data'].push({
              name: result['__companyName'],
              domain: result['items'][0]['displayLink']
            });
          }
        } catch (err) {
          _didIteratorError2 = true;
          _iteratorError2 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion2 && _iterator2.return) {
              _iterator2.return();
            }
          } finally {
            if (_didIteratorError2) {
              throw _iteratorError2;
            }
          }
        }

        results['length'] = values.length;
        ores.json(results);
      });
    }
  }, {
    key: 'serviceUrl',
    get: function get() {
      return this._url + '?key=' + this._apiKey + '&cx=' + this._cexId + '&num=' + this._NUM_RESULTS + '&fields=items(title,link,displayLink,formattedUrl),queries(request/searchTerms)';
    }
  }]);

  return GoogleCustomSearchService;
}();

exports.default = GoogleCustomSearchService;