
import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';

import util from 'util';


class CompanyDomainList extends React.Component {

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {
      data : [], length: 0
    };
  }

  render() {
    let companiesMarkup = <span className="gui-empty-state">No results to display</span>
    if(this.props.companies.data.length) {
      companiesMarkup = this.props.companies.data.map( (pair, idx) => {
        let { name, domain } = pair;
        return (
          <li key={idx}><strong>{name}</strong><a href={`http://${domain}`} target="_blank">{domain}</a></li>
        );
      });
    }
    return (
      <ul className="companies-list gui-2col-promotable">
        {companiesMarkup}
      </ul>
    );
  }
}


class MultiTextInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      value: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.parseCompanyArray = this.parseCompanyArray.bind(this);
    this.render = this.render.bind(this);
  }

  parseCompanyArray() {
    let companies_str = this.state.value || "";
    let companies_arr = [];
    if(!companies_str.length) {
      return companies_arr;
    }

    companies_arr = companies_str.split(',').map( (cval) => cval.trim()).filter( (cval) => Boolean(cval.length));

    return companies_arr;
  }

  handleChange(evt) {
    this.parseCompanyArray();
    this.setState({
      value: evt.target.value
    });
  }

  render() {

    return (
      <div className="gui-multitext-wrapper">
        <input type="text" className="gui-multitext" value={this.state.value} onChange={this.handleChange} />
        {this.parseCompanyArray().map( (comp, idx) => <input type="hidden" name={`${this.state.name}[]`} value={comp} key={`cidx_${idx}`} /> ) }
      </div>
    );

  }

}

class CompanyDomainFinder extends React.Component {

  constructor(props) {
    super(props);
    this.state = { value: 'nike,adidas', companies: { data: [], length: 0 }  };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(evt) {
    evt.preventDefault();
    let formData = jQuery("#company-form").serialize();
    console.log(`[FORM DATA] ${formData}`);
    let that=this;
    jQuery.ajax({
      url: `/url?${formData}`,
      success: (data) => {
        that.setState({ companies: data });
      },
      dataType: 'json'
    });
  }

  render() {

    return (
      <form id="company-form" onSubmit={this.handleSubmit} method="get" action="/url">
        <div className="gui-thinbanner">
          <strong>Enter up to 25 company names
            <small>Separate multiple business names with a comma</small>
          </strong>
          <MultiTextInput name="companies" />
        </div>
        <input type="submit" value="Find Domains" className="gui-btn-rich" />
        <div className="results-area gui">
          <CompanyDomainList companies={this.state.companies} />
        </div>
      </form>
    );

  }

}

export default CompanyDomainFinder;
