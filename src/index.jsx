
require('isomorphic-fetch');

import express from 'express';
import util from 'util';

import https from 'https';
import fs from 'fs';

import GoogleCustomSearchService from  './services';

const app = express();

// configuration parameters for the web server and google custom search
// @NOTE: Normally, I would place the CSE-identification variables
//        within an "environment-altering" script that would not
//        necessarily be pushed to the Git repository.
const CFG = {
  port: 3001,
  cse:  {
    url: "https://www.googleapis.com/customsearch/v1",
    apiKey: "AIzaSyBXGkVilmqEN0KSDAaQy1BlVVIA8r4nS6w",
    cexId: "009637816073108880163:nfsysoqnztc"
  }
};


// define static routes to expose
app.use(express.static('public/js', { }));
app.use(express.static('public/css', { }));

// define many entry point, and serve the 'index.html' file as the contents
app.get('/', (req, res) => {
    res.sendFile('index.html', { root: './public' }, (err) => {
      if(err) {
        console.log("Encountered error sending file::\n");
        console.log(util.inspect(err));
      } 
    });
});

// the workhorse API method, taking a GET request of 'company' variables and
// passing them into the "batch" function for the CSE service; within that
// method, each company corresponds to another API request to Google CSE
app.get('/url', (req, res) => {
  // ensure any errors not caught within a "promise chain" don't percolate
  // up the stack further than this scope
  try {
    // set response content-type, since we know it's going to be serving JSON
    res.set('Content-Type', 'application/json');
    // instantiate the custom 'GoogleCustomSearchService' class, passing in
    // the API configuration parameters
    let searchService = new GoogleCustomSearchService(
      CFG['cse']['url'],
      CFG['cse']['apiKey'],
      CFG['cse']['cexId']
    );
    let qstr = req.query;
    // if query string indicates presence of required 'company', potentially multi-valued variable
    if(qstr.hasOwnProperty('companies')) {
      let companies = qstr.companies;
      // ensure the provided 'company' GET variable is NOT empty
      if(companies.length) {
        // if more than 25 companies provided, cut off the values at 25 per the project requirement limit
        if(companies.length > 25) {
          // @NOTE: Under normal conditions, this would likely be first handled by error-checking
          //        on the front-end, within the UI, to avoid unnecessary server calls and provide
          //        a "quicker", and therefore better, user experience.
          companies = companies.slice(0,25);
        }
        // perform the batch query to Google CSE, passing the companies and the 'res'ponse object,
        // as the results from the query will completely fill out / handle the response object.
        searchService.batchQuery(companies, res);
      }
    } else {
      console.log("[WARNING] No 'companies' provided to url '/url' service.");
    }
  } catch(err) {
    // @NOTE: Ideally would handle this with something more streamlined, likely including an
    //        always-persistent "status" flag or equivalent within the responses, so the
    //        UI can act accordingly to show the error.
    res.json({ status: false });
  }
})

app.listen(CFG.port, () => console.log(`Listening on port ${CFG['port']}`));
