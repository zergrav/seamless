
import React from 'react';
import ReactDOM from 'react-dom';

import CompanyDomainFinder from './app';

ReactDOM.render(
  <CompanyDomainFinder />,
  document.getElementById('page-app')
);

