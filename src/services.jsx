
require('isomorphic-fetch');

import express from 'express';
import util from 'util';

import https from 'https';
import fs from 'fs';

class GoogleCustomSearchService {

  constructor(url, apiKey, cexId) {
    this._url = url;
    this._apiKey = apiKey;
    this._cexId = cexId;
    this._NUM_RESULTS = 1;
  }

  get serviceUrl() {
    return `${this._url}?key=${this._apiKey}&cx=${this._cexId}&num=${this._NUM_RESULTS}&fields=items(title,link,displayLink,formattedUrl),queries(request/searchTerms)`;
  }

  querySingle(company, outerRes) {
    let companyStr = company;
    let companyQuery = `"${encodeURIComponent(company.trim())}"`
    let requestUrl = `${this.serviceUrl}&q=${companyQuery}`;
    let promise = new Promise((resolve, reject) => {
      let data = "";
      https.request(requestUrl, (res) => {
        res.on('data', (d) => {
          data += d;
        })
        res.on('end', () => {
          let dataObj = JSON.parse(data);
          dataObj['__companyName'] = companyStr;
          resolve(dataObj);
        });
      })
      .on('error', (e) => {
        reject(`Received Promisifid query error: ${String(e)}`);
      })
      .end();
    });
    return promise;
  }

  batchQuery(companies, outerRes) {
    let pArr = [];
    let ores = outerRes;

    for(let company of companies) {
      let qpromise = this.querySingle(company, outerRes);
      pArr.push(qpromise);
    }

    let companyCount = companies.length;

    Promise.all(pArr).then( (values) => {
      let results = { data: [], length: 0 };
      for(let result of values) {
        results['data'].push({
          name: result['__companyName'],
          domain: result['items'][0]['displayLink']
        });
      }
      results['length'] = values.length;
      ores.json(results);
    });
  }
}

export default GoogleCustomSearchService;
