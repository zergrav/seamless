#!/bin/bash

__RUN_NODE() {
  declare -r SCRIPT_PATH="$(readlink -e "$0")";
  declare -r SCRIPT_DIR="$(dirname "$SCRIPT_PATH")";

  pushd . &>/dev/null;

  cd "$SCRIPT_DIR";

  nodejs . | tee "output.log";

  popd &>/dev/null;

  return 0;
}

__RUN_NODE;

